# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('storage', models.FileField(upload_to=b'static')),
                ('name', models.CharField(max_length=512)),
                ('type', models.CharField(max_length=512)),
            ],
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('submitter_email', models.EmailField(max_length=254)),
                ('submitter_name', models.CharField(max_length=20)),
                ('consent', models.BooleanField()),
                ('comment', models.TextField()),
                ('timestamp', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Confirmation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=64, blank=True)),
                ('email', models.EmailField(max_length=254)),
            ],
        ),
        migrations.CreateModel(
            name='FeaturedCase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Operator',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256)),
                ('logo', models.ImageField(null=True, upload_to=b'operator_logo', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Violation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country', models.CharField(max_length=2, choices=[(b'BE', 'Belgium'), (b'BG', 'Bulgaria'), (b'CZ', 'Czech Republic'), (b'DK', 'Denmark'), (b'DE', 'Germany'), (b'EE', 'Estonia'), (b'IE', 'Ireland'), (b'EL', 'Greece'), (b'ES', 'Spain'), (b'FR', 'France'), (b'IS', 'Iceland'), (b'IT', 'Italy'), (b'CY', 'Cyprus'), (b'LV', 'Latvia'), (b'LT', 'Lithuania'), (b'LU', 'Luxembourg'), (b'HU', 'Hungary'), (b'MT', 'Malta'), (b'NL', 'Netherlands'), (b'NO', 'Norway'), (b'AT', 'Austria'), (b'PL', 'Poland'), (b'PT', 'Portugal'), (b'RO', 'Romania'), (b'SI', 'Slovenia'), (b'SK', 'Slovakia'), (b'FI', 'Finland'), (b'SE', 'Sweden'), (b'UK', 'United Kingdom')])),
                ('contract', models.CharField(max_length=256, blank=True)),
                ('resource', models.CharField(blank=True, max_length=20, choices=[(b'port', 'port'), (b'protocol', 'protocol'), (b'service', 'service'), (b'site', 'site'), (b'user', 'user'), (b'ip', 'ip')])),
                ('resource_name', models.CharField(max_length=4096, blank=True)),
                ('type', models.CharField(blank=True, max_length=20, choices=[(b'blocking', 'Blocking'), (b'throttling', 'Throttling')])),
                ('media', models.CharField(blank=True, max_length=20, choices=[(b'fixed', 'Fixed'), (b'mobile', 'Mobile')])),
                ('temporary', models.BooleanField()),
                ('contractual', models.BooleanField()),
                ('contract_excerpt', models.TextField(blank=True)),
                ('loophole', models.BooleanField()),
                ('activationid', models.CharField(max_length=128, blank=True)),
                ('state', models.CharField(default=b'new', max_length=20, blank=True, choices=[(b'moreinfo', 'Need more info'), (b'new', 'New'), (b'verified', 'Verified'), (b'duplicate', 'Duplicate'), (b'ooscope', 'Out of scope'), (b'closed', 'Closed')])),
                ('editorial', models.TextField(blank=True)),
                ('operator_ref', models.ForeignKey(related_name='violations', to='bt.Operator')),
            ],
        ),
        migrations.AddField(
            model_name='featuredcase',
            name='case',
            field=models.OneToOneField(to='bt.Violation'),
        ),
        migrations.AddField(
            model_name='confirmation',
            name='violation',
            field=models.ForeignKey(to='bt.Violation'),
        ),
        migrations.AddField(
            model_name='comment',
            name='violation',
            field=models.ForeignKey(to='bt.Violation'),
        ),
        migrations.AddField(
            model_name='attachment',
            name='comment',
            field=models.ForeignKey(to='bt.Comment'),
        ),
    ]
