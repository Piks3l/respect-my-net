from haystack import indexes
from models import Operator, Violation

class ViolationIndexes(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    operator = indexes.CharField(model_attr="operator_ref")
    country = indexes.CharField(model_attr="country")
    contract = indexes.CharField(model_attr="contract_excerpt")
    type = indexes.BooleanField(model_attr="contractual")
    operator_name = indexes.CharField()

    def get_model(self):
        return Violation

    def prepare_operator_name(self, obj):
        return obj.operator_ref.name
