function focushandlers(ed) {
	var s = ed.settings;
	var realID = $('#'+ed.id+'_tbl');
	tinymce.dom.EventUtils.bind(ed.getWin(), 'focus', function(e) {
		if($(realID)) {
			//console.log(realID);
			realID.parents('.fieldWrapper').find('.help_text').show();
		}
	});
	tinymce.dom.EventUtils.bind(ed.getWin(), 'blur', function(e) {
		if($(realID)) {
			//console.log(realID);
			realID.parents('.fieldWrapper').find('.help_text').hide();
		}
	});
}

// Forms
// This is a test to see if we hav an empty field
function empty(node) {
	if(node.text().trim()) return false;
	return true;
}

// This fucntiosn check the first four forms
function check1st4() {
	var country=$('#id_country').val();
	var op=$('#id_operator').val();
	var contract=$('#id_contract').val();
	var media=$('#id_media').val();
	var div=$('#similar_cases');
	if(country && op && contract && media) {
		$.getJSON('/lookup/?country='+country+'&contract='+contract+'&operator='+op+'&media='+media, function(data) {
			if (data.length>0) {
				div.parent().removeClass('hidden');
			} else {
				div.parent().addClass('hidden');
			}
			while(data) {
				var val=data.pop();
				div.append('<li><a href="/view/'+val[0]+'">'+(val[1] || "no resource given")+'</a></li>');
			};
		});
		$('#id_comment').parent().parent().show().focus();
		$('#id_comment_ifr').focus();
		$('#id_email').parent().parent().show();
		$('#id_consent').parent().parent().show();
		$('#id_nick').parent().parent().show();
		$('#id_attachments0').parent().parent().show();
		$("#save_button").attr('disabled','true').show();
	} else {
		div.parent().addClass('hidden');
	}
}

// Initialise the form
function init_form() {
	$('.help_text').hide();
	$(".fieldWrapper").focusin(function(event) {if(!empty($(this).find('.help_text'))) $(this).find('.help_text').show();});
	$(".fieldWrapper").focusout(function(event) {if(!empty($(this).find('.help_text'))) $(this).find('.help_text').hide();});
	$("#show_optionals").show();
	$("#id_contractual").click(function(event) {
		$("#id_contract_excerpt_parent").parent().parent().toggle();
	});
	$("#show_optionals").click(function(event) {
		$(this).hide();
		$("#optional .fieldWrapper").show();
		$("#id_contract_excerpt_parent").parent().parent().hide();
	});
	$('#id_country').change(function() { check1st4(); });
	$('#id_operator').autocomplete({
		source: function( request, response ) {
			if(request.term.length>0) {
				var country=$('#id_country').attr('value');
				$.getJSON('/api/operators/?name__startswith='+request.term, function(data) { 
					var res=[];
					for(var i=0;i<data.objects.length;i++) {
						res.push(data.objects[i].name);
					}
					response(res);
				});
			}
		}
	});
	$('#id_contract').autocomplete({
		source: function( request, response ) {
			if(request.term.length>0) {
				var operator=$('#id_operator').attr('value');
				var country=$('#id_country').attr('value');
				$.getJSON('/api/violations/?country='+country+'&operator_ref__name='+operator+'&contract__contains='+request.term, function(data) { 
					var res=[];
					for(var i=0;i<data.objects.length;i++) {
						res.push(data.objects[i].contract);
					}
					response(res);
				});
			}
		}
	});
	$('#id_operator').change(function() { check1st4(); });
	$('#id_contract').change(function() { check1st4(); });
	$('#id_media').change(function() { check1st4(); });
	$('#id_email').change(function() {
		if($(this).val().length>0) {
			$('#id_captcha_0').parent().parent().show();
			$('#save_button').removeAttr('disabled');
			$("#show_optionals").show();
		}
	});
	$("form").bind("keypress", function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			$(e.target).change();
			return false;
		}
		return true;
	});
};
// END Form

$(document).ready(function() {
	// Maps scripts are here
	// This function redirects you to the country you clicked on.
	var onCountryClick = function(target) {
		window.location='/list/'+target.id+"/";
	};
	// Do the maps calculations, and configure it
	/* we're gonna calculate the colors for our countries */
	var dataset = {}
	var onlyValues = data.map(function(obj){ return obj.w});
	var minValue = Math.min.apply(null, onlyValues),
	maxValue = Math.max.apply(null, onlyValues);
	var paletteScale = d3.scale.linear()
		.domain([minValue, maxValue])
		.range(['#EFEFEF', '#02386F']);

	// Let's flatten the data bits.
	data.forEach(function(item) {
		var iso = item.iso2,
		value = item.w;
		dataset[iso] = { numberOfCases: value, fillColor: paletteScale(value)};
	});

	// If we're on a single coutry map, let's build a dataset which will
	// highlight the country.
	if (country) {
		dataset[country] = {fillColor: '#02386F'};
	}

	// Do the maps works
	$('#map').datamaps({
		element: document.getElementById('map'),
		scope: 'europe',
		setProjection: function(element, options) {
			var projection, path;
			projection = d3.geo.stereographic()
				.center([15.22574, 54.2361])
				.scale(element.offsetWidth * 2)
				.translate([element.offsetWidth / 2, element.offsetHeight / 2]);
			path = d3.geo.path()
				.projection(projection);
			return {path: path, projection: projection}
		},
		data: dataset,
		fills: {defaultFill: '#F5F5F5'},
		geographyConfig: {
			dataUrl: '/static/europe.topojson',
			borderColor: '#DEDEDE',
			highlightBorderWidth: 2,
			highLightFillColor: function(geo) {
				return geo['fillColor'] || '#F5F5F5';
			},
			highlightBorderColor: '#B7B7B7',
			popupTemplate: function(geo, data) {
				if (!data){ return };
				return ['<div class="hoverinfo">',
					'<strong>', geo.properties.NAME, '</strong>',
					'<br><strong>', data.numberOfCases, '</strong> Reported cases',
					'</div>'].join('')
			}
		},
		done: function(datamap) {
			datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
				window.location='/list/'+geography.id+"/";
			});
		}
	});
	// END maps
	// Forms
	init_form();
	$('.confirm_form').submit(function() {
		var self=this;
		$.ajax({url: '/confirm/'+$(this).attr('id')+'/'+$(this).children('input:first').attr('value'), success: function(data) { $(self).html(data); }});
		return false;
	});
	tinyMCE.init({
		mode : "textareas",
		width : "284",
		height : "140",
		theme : "modern",
		theme_advanced_toolbar_align : "left",
		theme_advanced_toolbar_location : "top",
		theme_advanced_buttons1 : "",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		plugins : "paste",
		paste_auto_cleanup_on_paste : true,
		paste_strip_class_attributes: 'all',
		paste_remove_styles: true,
		init_instance_callback: focushandlers,
		valid_elements : "@[id|title|dir<ltr?rtl|lang|xml::lang],a[rel|rev|"
			+ "charset|hreflang|name|href|title],strong/b,em/i,"
			+ "strike,u,p,-ol,-ul,-li,br,-sub,-sup,-blockquote,"
			+ ",-code,-pre,address,-h1,-h2,-h3,-h4,-h5,"
			+ "-h6,hr[size|noshade],dd,dl,dt,cite,abbr,acronym,"
			+ "del[datetime|cite],ins[datetime|cite]",
	});
	$(".confirm").click(function(){
		$(this).parent().parent().find('.info-validate').toggle();
	});
	$(".cancel-button").click(function(){
		$(this).parent().parent().hide();
	});
	// END Forms
	$("#sortedlist").tablesorter({
		headers: { 8: { sorter: false },
			0: { sorter: 'stateparser' }},
			sortList: [[1,0],[0,0],[7,1]],
	});
	var options = { enableCookies: false };
	$('#sortedlist').tableFilter(options);
	$('.confirm_form').submit(function() {
		var self=this;
		var email=$(this).find('input:first').attr('value');
		if( email.length>0) {
			$.ajax({url: '/confirm/'+$(this).attr('id').slice(1)+'/'+email, success: function(data) { $(self).html(data); }});
		} else {
			$(this).find('[type=text]').focus();
		}
		return false;
	});
	$(".confirm > .button").click(function(){
		$(this).parent().parent().parent().find('.info-validate').toggle();
	});
	$(".cancel-button").click(function(){
		$(this).parent().hide();
	});
});
